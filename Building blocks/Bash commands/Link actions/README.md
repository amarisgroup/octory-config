# Bash command - Open link

This command will open a link when the user clicks on a button. The configuration file show two buttons with different links.

## Open mail client with address

To be filled:
- Email Address

#### Xml 
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Type</key>
	<string>Button</string>
	<key>Text</key>
	<string>Contact IT</string>
	<key>Style</key>
	<string>Simple</string>
	<key>Alignment</key>
	<string>Center</string>
	<key>OnClick</key>
	<dict>
		<key>Type</key>
		<string>Parallel</string>
		<key>Actions</key>
		<array>
			<dict>
				<key>Type</key>
				<string>ExecuteCommand</string>
				<key>Command</key>
				<string>open mailto:#EmailAddress#</string>
			</dict>
		</array>
	</dict>
</dict>
</plist>
```

#### Plist Editor

Copy and paste the following inside Plist Editor to use it directly.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Type</key>
	<string>Button</string>
	<key>Text</key>
	<string>Contact IT</string>
	<key>Style</key>
	<string>Simple</string>
	<key>Alignment</key>
	<string>Center</string>
	<key>OnClick</key>
	<dict>
		<key>Type</key>
		<string>Parallel</string>
		<key>Actions</key>
		<array>
			<dict>
				<key>Type</key>
				<string>ExecuteCommand</string>
				<key>Command</key>
				<string>open mailto:support.it@mycompany.com</string>
			</dict>
		</array>
	</dict>
</dict>
</plist>
```

<br/>

## Open a web page

Can a be for example a ticketing tool, a home page...

To be filled:
- Web page

#### Xml 
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Type</key>
	<string>Button</string>
	<key>Text</key>
	<string>Contact IT</string>
	<key>Style</key>
	<string>Simple</string>
	<key>Alignment</key>
	<string>Center</string>
	<key>OnClick</key>
	<dict>
		<key>Type</key>
		<string>Parallel</string>
		<key>Actions</key>
		<array>
			<dict>
				<key>Type</key>
				<string>ExecuteCommand</string>
				<key>Command</key>
				<string>open https://#WebPage#</string>
			</dict>
		</array>
	</dict>
</dict>
</plist>
```

#### Plist Editor

Copy and paste the following inside Plist Editor to use it directly.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Type</key>
	<string>Button</string>
	<key>Text</key>
	<string>Contact IT</string>
	<key>Style</key>
	<string>Simple</string>
	<key>Alignment</key>
	<string>Center</string>
	<key>OnClick</key>
	<dict>
		<key>Type</key>
		<string>Parallel</string>
		<key>Actions</key>
		<array>
			<dict>
				<key>Type</key>
				<string>ExecuteCommand</string>
				<key>Command</key>
				<string>open https://ticketing.mycompany.com</string>
			</dict>
		</array>
	</dict>
</dict>
</plist>
```