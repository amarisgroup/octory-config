# Bash command - Local IP address

This command will retrieve the local IP address of the device when the app is launched, and store it inside a variable named `LocalIP`.

The plist file is given as an example to show you how the variable `LocalIP` can be used.

#### Xml

```xml
<key>ActionSets</key>
<array>
	<dict>
		<key>Type</key>
		<string>Parallel</string>
		<key>Triggers</key>
		<array>
			<string>Launch</string>
		</array>
		<key>Actions</key>
		<array>
			<dict>
				<key>Type</key>
				<string>ExecuteCommand</string>
				<key>Command</key>
				<string>ifconfig | grep "inet" | head -n 2 | tail -n 1 | cut -d " " -f 2</string>
				<key>Variable</key>
				<string>LocalIP</string>
			</dict>
		</array>
	</dict>
</array>
```
#### Plist Editor

Copy and paste the following inside Plist Editor to use it directly.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
	<dict>
		<key>Type</key>
		<string>Parallel</string>
		<key>Triggers</key>
		<array>
			<string>Launch</string>
		</array>
		<key>Actions</key>
		<array>
			<dict>
				<key>Type</key>
				<string>ExecuteCommand</string>
				<key>Command</key>
				<string>ifconfig | grep &quot;inet &quot; | head -n 2 | tail -n 1 | cut -d &quot; &quot; -f 2</string>
				<key>Variable</key>
				<string>LocalIP</string>
			</dict>
		</array>
	</dict>
</array>
</plist>
```