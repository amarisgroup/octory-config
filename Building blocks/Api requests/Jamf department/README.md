# Api request - Jamf department

This request will change a computer`#Department#` using the classic Jamf API.
To use it, add a `SendRequest` action inside an [ActionSet](https://documents.getceremony.app/Actions.html#action-sets), which will for example be executed when the application is launched or when the user clicks a button.

The plist file is given as an example to show you how the Api request can be used.

### To be filled:
- Jamf Api URL
- Department

#### Xml

```xml
<key>APIRequests</key>
<dict>
	<key>JamfBaseURL</key>
	<string>https://#JAMF_API_URL#/JSSResource/</string>
	<key>Models</key>
	<dict>
		<key>ExtensionAttribute</key>
		<dict>
			<key>Endpoint</key>
			<string>computers/serialnumber/${DEVICE_SERIAL_NUMBER}</string>
			<key>MDMApi</key>
			<string>Jamf</string>
			<key>Method</key>
			<string>PUT</string>
			<key>Body</key>
			<dict>
				<key>computer</key>
				<dict>
					<key>location</key>
					<dict>
						<key>department</key>
						<string>#Department#</string>
					</dict>
				</dict>
			</dict>
		</dict>
	</dict>
</dict>
```

#### Plist Editor

Copy and paste the following inside Plist Editor to use it directly.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>JamfBaseURL</key>
	<string>https://#JAMF_API_URL#/JSSResource/</string>
	<key>Models</key>
	<dict>
		<key>ExtensionAttribute</key>
		<dict>
			<key>Endpoint</key>
			<string>computers/serialnumber/${DEVICE_SERIAL_NUMBER}</string>
			<key>MDMApi</key>
			<string>Jamf</string>
			<key>Method</key>
			<string>PUT</string>
			<key>Body</key>
			<dict>
				<key>computer</key>
				<dict>
					<key>location</key>
					<dict>
						<key>department</key>
						<string>#Department#</string>
					</dict>
				</dict>
			</dict>
		</dict>
	</dict>
</dict>
</plist>
```