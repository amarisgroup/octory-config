#  Form and Apps Monitoring

## Notes

The inputs are stored by default at *~/Octory/Octory_inputs.json*. You can choose the file format (JSON, PLIST) and the file path where to save the input file in the [Input section](https://documents.getceremony.app/ConfigurationFile.html#input-dictionary) of the configuration plist   

## Slide

The user needs to enter some info . A validation on some inputs checks that they entered correct info before they can close the window with the "Quit" button.
Custom fonts and colors are applied.

![Slide](Slide1.png)
![Slide with warning messages](Slide1-invalid.png)
