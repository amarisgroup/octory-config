#  Form and Apps Monitoring

## Warning ⚠️

Before launching Octory with this preset, do note the following shortcuts to exit the application:

- **⌥⌘M** to reach the end of monitoring simulation, and thus simulating that all monitors are installed
- **⌘Q** to exit the app when all monitors are installed (or simulated so)

So basically just use **⌥⌘M**  then **⌘Q** to exit. Or use the force quit shortcut **⇧⌃⌥⌘F**.

## Notes
- Simulate the installation of the monitors with **⇧⌘M**.  You can reach the end of the simulation with **⌥⌘M**.
- The left container uses the `WidthRatio` key with a value set to 0.8 to take 70% of the slide width. The right container uses the remaining 20% to show the app monitoring.
- The ”Evernote” monitor is marked as [”Not mandatory”](https://documents.octory.io/Monitoring.html#application-monitoring), which means that it is not required to install it successfully to let the user exit the app.
- Some monitors icon are provided in the resources, while others are retrieved from the application files directly when the app is installed.

## Slide

A web view with an app monitoring view. Octory is in full screen and the navigation view shows a "Quit" button.  

![Slide1](Slide1.png)
