
# Update OS

We were asked several times by customers if Octory could replace or act as [Nudge](https://github.com/macadmins/nudge). The answer is yes.  
This preset will allow you to launch an Octory window to inspire your users to update their Mac. By default, this preset will launch Octory in full screen so users have to take actions. After the due date, users will not be able to defer the update. Their only choice will be to update.  
  
  **Octory PRO is required.**


## Demo

Screenshot of the preset. You can directly edit it with the PLIST Editor of your choice.
![UpdateOS](UpdateOS.png)
## Deployment

This preset is a combination of two files. The Octory plist config file and a LaunchAgent. The configuration file allow you to fully configure the Octory popup while the LaunchAgent let you decide when and how often the popup should appear.  
  
We won't go into detail on LaunchAgents here but you can create your own using [This Generator](https://launched.zerowidth.com). The one provided with the template will launch the app every 30min.

### Changes to the PLIST file
To adapt the config file to your liking, you have to modify at least 2 keys.  
  
  *ActionSets[0].Actions[1]*  
  Modify the date using the format YYYYmmddHHMMSS. This date correspond to the day the app will force the user to update (no defer possible). As an example, for October 12, 2023 at 10am you should have 20231012100000. 
![UpdateOS](UpdateOS_ForceUpdateDay.png)
```xml
    <key>Command</key>
    <string>date -j -f %Y%m%d%H%M%S 20220216000000 +%Y-%m-%d</string>
```

*General.Variables.UpdateDay*  
Modify the date using the same format as above to chose the last day before the user is forced to update.
![UpdateOS](UpdateOS_UpdateDay.png)
```xml
    <key>UpdateDay</key>
	<string>20220216000000</string>
```