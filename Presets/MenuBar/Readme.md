#  Menu bar preset

To use the Menu bar window,  the value of the key *Window*→`OnScreen` has to be set to "MenuBar". Then, it is required to add the section *MenuBarSlide* which contains one slide. This slide will be the one displayed when the user clicks on the menu bar icon.

### Notes

- The buttons to copy the informations execute a `CopyToClipboard` action which requires Octory PRO. The same goes for the button opening the mail client or the web page.
- The local IP address is retrieved by a bash command in the `ActionSets` section, and thus requires Octory PRO too.

![Slide1-dark](Slide1-dark.png)
![Slide1-light](Slide1-light.png)