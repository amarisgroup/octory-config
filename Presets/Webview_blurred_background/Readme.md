#  Form and Apps Monitoring

## Warning ⚠️

Before launching Octory with this preset, do note the following shortcuts to exit the application:

- **⌥⌘M** to reach the end of monitoring simulation, and thus simulating that all monitors are installed
- **⌘Q** to exit the app when all monitors are installed (or simulated so)

So basically just use **⌥⌘M**  then **⌘Q** to exit. Or use the force quit shortcut **⇧⌃⌥⌘F**.

## Slide

A web view with an app monitoring view. Octory is in blurred background. The **Quit button** displays a customised text. When simulating apps installation with **⇧⌘M**, the quit button becomes enable only at the end of the simulation. You can reach the end with **⌥⌘M**.

- The installation steps text uses [placeholders](https://documents.octory.io/Variables.html) available in Octory 2.1.0+.
- Some monitors icon are provided in the resources, while others are retrieved from the application files directly when the app is installed.

![Slide1](Slide1.png)