#  High-quality images

Useful when the text content is short and you want to propose a graphic experience.

![Slide1](Slide1.png)

![Slide2](Slide2.png)