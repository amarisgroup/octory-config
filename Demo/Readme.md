# Demos
Those files are used to demo two use cases of Octory an onboarding workflow and a helper workflow in the menu bar.
PRO versions of the configurations are also provided to better understand the PRO features and how they can enhance the standard usage of Octory.
(Only the light mode is displayed in the screenshots)

## Onboarding
![](Screenshots/onboarding-slide1.png)
![](Screenshots/onboarding-slide2.png)

## Onboarding PRO
![](Screenshots/onboarding-pro-slide1.png)
![](Screenshots/onboarding-pro-slide2.png)
![](Screenshots/onboarding-pro-slide3.png)

## MenuBar
![](Screenshots/menubar-slide-1.png)

## MenuBar PRO
![](Screenshots/menubar-pro-slide-1.png)