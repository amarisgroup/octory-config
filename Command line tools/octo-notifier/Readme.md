# octo-notifier

This command-line tool is an interface to send distributed notifications to Octory.

The notifier has been moved to our [downloads page](https://www.octory.io/downloads).

## Auto-completion
To install the tool auto-completion, run
```bash
octo-notifier install-completion-script
```

It might be needed ro source your shell and to restart the terminal app.

## Update a monitor
It’s possible to send an app monitor update with the `monitor` command. The name and the installation state of the monitor should be provided.

```bash
octo-notifier monitor Xcode -s installed
```

## Send a custom trigger event
With Octory `2.1.0+`, it’s possible to declare a custom trigger to execute an action set. Thus by sending a distributed notification to Octory, it’s possible to execute the action set.
(The `ButtonComponent` offers a similar feature in `2.1.0`).

Note: actions sets are part of Octory PRO.

To send a custom trigger event, use the `trigger` command followed by the trigger name.

```bash
octo-notifier trigger finish
```

The declaration of a custom trigger is explained in the [Triggers](https://documents.octory.io/Actions.html#triggers) section in the wiki.
