# config

This command-line tool is an interface for the library used in Octory to encode/decode configuration files. You can use it to validate a configuration file in the terminal or in your scripts to improve your workflows.

To use it, simply run `config check [config-file]`.

## Remarks
- The tool is *not notarized* yet, but *is signed*.
- The tool version corresponds to the Octory version to use for the configuration format. Once unzipped, run `config --version` to get the tool version.