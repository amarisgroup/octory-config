# OctoryConfig

This a repository to gather Octory's configurations.

## Use a configuration
To use a configuration file, move it to one of the two following folders: */Library/ManagedPreferences* and */Library/Application Support/Octory*. You can then launch Octory while specifying a specific configuration with the `--configuration | -c ` argument. This argument takes a relative path to the configuration file, using the mentioned folders as a base.

For example, to launch the Octory demo configuration named *menubar-pro.plist* at the path */Library/Application Support/Octory/Demo/menubar-pro.plist* you could write:

```bash
open Octory.app --args -c Demo/menubar-pro
```

Or

```bash
Octory.app/Contents/MacOS/Octory -c Demo/menubar-pro
```

Last example, to launch Octory with the School preset configuration at the path */Library/ManagedPreferences/Presets/School/Octory.plist*, you would write:

```bash
open Octory.app --args -c Presets/School/Octory
```

## Contributing

### How to

You are welcome to contribute! Willing to add a new building block? Or you think that your configuration might be useful as a preset? Here is how to do it.

#### Reach out to us

You can send your contribution to the Amaris team on the [Octory slack channel](https://macadmins.slack.com/archives/CJFH6NJAX) or send an email to [Octory Team](mailto:octory.contact@amaris.com).

#### Fork

Otherwise, you can do it with the following method.

- Fork the project

![](ReadmeScreenshots/fork.png)

- Copy the url of the project and clone it:

![](ReadmeScreenshots/clone.png)

```bash
 git clone https://gitlab.com/${USER_NAME}/octory-config.git
```

Add your files in one of the folders, then commit and push them:

```bash
$ git commit -m "Awesome new preset by Mr. Duck"
$ git push
```

Then open a **Merge request**, from your fork master branch to the [Amaris octory-base one](https://gitlab.com/amarisgroup/octory-config):

![](ReadmeScreenshots/merge-request.png)

![](ReadmeScreenshots/merge-request-comp.png)

Enter a title and a description. Please try to be as much descriptive as possible. Also, tag Benjamin Richard for us to review your proposition.

![](ReadmeScreenshots/merge-request-details.png)

You can then submit your Merge request. We will then review it, and post some comments if necessary. When everything is ready, your merge request will be validated and your propositions merged into the Amaris repository master branch.


### Some rules

- Do not put sensitive information from you or your company in those files. Amaris will not take any responsibility if such a piece of information is made public in this repository.
- If possible, it is better to share the resources you use in the configuration.
- **Always add a Readme** to explain what is the configuration for, and if necessary how to use it.
- In the Readme, please write your name and a way to reach you if other users find it useful.
