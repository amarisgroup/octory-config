#!/bin/zsh
# to be executed when Octory has just succesffully completed

# Unload Octory launch agent
loggedInUser=$(echo "show State:/Users/ConsoleUser" | scutil | awk '/Name :/ && ! /loginwindow/ { print $3 }')
loggedInUID=$(id -u ${loggedInUser})
/bin/launchctl bootout gui/${loggedInUID}/com.amaris.octory.launch

# Create Octory done file for the user
/usr/bin/touch "/Users/${loggedInUser}/Library/Preferences/OctoryDone"

exit 0
